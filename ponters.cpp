#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;
    /* Program porównujący szybkość działania na tablicy bez wskaźników oraz z ich użyciem
    1. Zarezerwuj sporą tablicę intów
    2. Na każdej szufladce dokonaj:
       - zapisu liczby
       - zwiększenia tej liczby o 50
    3. Wykonaj punkt 2 na dwa sposoby:
       - bez wskaźnikóœ
       - ze wskaźnikami
    4. Za każdym razem zmierz czas
    */
int main()
{
    int ile;
    clock_t start, stop; // posłużą do uruchomienia i zatrzymania odliczania czasu, clock_t to typ
    double czas; // zmienna do przechowywania czasu
    cout << "Ile liczb chcesz mieć w tablicy: ";
    cin >> ile;

    // bez wskaźnika
    int *tablica;
    tablica = new int [ile];
    start = clock(); // pobiera liczbę cykli procesora
    for (int i = 0; i < ile; i++) {
        tablica[i] = i;
        tablica[i] += 50;
    }
    stop = clock();
    czas = (double)(stop - start) / CLOCKS_PER_SEC; // ile minęło czasu + zamiana na sekundy, sam pobiera z systemu
    cout << "Czas zapisu (bez wskaźnika): " << czas << endl;
    delete [] tablica;

    // ze wskaźnikiem
    int *wskaznik = tablica;
    tablica = new int [ile];
    start = clock();
    for (int i = 0; i < ile; i++) {
        *wskaznik = i;
        *wskaznik += 50;
    }
    stop = clock();
    czas = (double)(stop - start) / CLOCKS_PER_SEC; // ile minęło czasu + zamiana na sekundy, sam pobiera z systemu
    cout << "Czas zapisu (ze wskaźnikiem): " << czas << endl;
    delete [] tablica;

    return 0;
}

