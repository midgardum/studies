#ifndef FIGURE_H
#define FIGURE_H
#include <iostream>

// klasa bazowa
class Figure
{
public:
    virtual double calculate() = 0;
    virtual void get() = 0;
    virtual std::string fig_name() = 0;
    virtual ~Figure() = default;
};

#endif // FIGURE_H
