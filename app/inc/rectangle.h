#ifndef RECTANGLE_H
#define RECTANGLE_H
#include "figure.h"
#include <iostream>

class Rectangle : public Figure
{
public:
    Rectangle() = default;
    Rectangle(const Rectangle&) = delete;
    Rectangle operator=(const Rectangle&) = delete;
    Rectangle(Rectangle&&) = delete;
    Rectangle operator=(Rectangle&&) = delete;
    ~Rectangle() = default;
    double calculate() override;
    void get() override;
    std::string fig_name() override;
private:
    double a;
    double b;
    double total;
    std::string name;
};

#endif // RECTANGLE_H
