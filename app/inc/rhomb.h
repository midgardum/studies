#ifndef RHOMB_H
#define RHOMB_H
#include "figure.h"
#include <iostream>

class Rhomb : public Figure
{
public:
    Rhomb() = default;
    Rhomb(const Rhomb&) = delete;
    Rhomb operator=(const Rhomb&) = delete;
    Rhomb(Rhomb&&) = delete;
    Rhomb operator=(Rhomb&&) = delete;
    ~Rhomb() = default;
    double calculate() override;
    void get() override;
    std::string fig_name() override;
private:
    double d1;
    double d2;
    double total;
    std::string name;
};

#endif // RHOMB_H
