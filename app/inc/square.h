#ifndef SQUARE_H
#define SQUARE_H
#include "figure.h"
#include <iostream>

class Square : public Figure
{
public:
    Square() = default;
    Square(const Square&) = delete;
    Square operator=(const Square&) = delete;
    Square(Square&&) = delete;
    Square operator=(Square&&) = delete;
    ~Square() = default;
    double calculate() override;
    void get() override;
    std::string fig_name() override;
private:
    double a;
    double total;
    std::string name;

};

#endif // SQUARE_H
