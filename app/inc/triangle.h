#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "figure.h"
#include <iostream>

class Triangle : public Figure
{
public:
    Triangle() = default;
    Triangle(const Triangle&) = delete;
    Triangle operator=(const Triangle&) = delete;
    Triangle(Triangle&&) = delete;
    Triangle operator=(Triangle&&) = delete;
    ~Triangle() = default;
    double calculate() override;
    void get() override;
    std::string fig_name() override;
private:
    double a;
    double h;
    double total;
    std::string name;
};

#endif // TRIANGLE_H
