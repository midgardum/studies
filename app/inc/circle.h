#ifndef CIRCLE_H
#define CIRCLE_H
#include "figure.h"
#include <iostream>

class Circle : public Figure
{
public:
    Circle() = default;
    Circle(const Circle&) = delete;
    Circle operator=(const Circle&) = delete;
    Circle(Circle&&) = delete;
    Circle operator=(Circle&&) = delete;
    ~Circle() = default;
    double calculate() override;
    void get() override;
    std::string fig_name() override;
private:
    double r;
    double total;
    std::string name;
};

#endif // CIRCLE_H
