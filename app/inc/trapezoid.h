#ifndef TRAPEZOID_H
#define TRAPEZOID_H
#include "figure.h"
#include <iostream>

class Trapezoid : public Figure
{
public:
    Trapezoid() = default;
    Trapezoid(const Trapezoid&) = delete;
    Trapezoid operator=(const Trapezoid&) = delete;
    Trapezoid(Trapezoid&&) = delete;
    Trapezoid operator=(Trapezoid&&) = delete;
    ~Trapezoid() = default;
    double calculate() override;
    void get() override;
    std::string fig_name() override;
private:
    double a;
    double b;
    double h;
    double total;
    std::string name;
};


#endif // TRAPEZOID_H
