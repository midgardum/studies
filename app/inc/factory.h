#ifndef FACTORY_H
#define FACTORY_H
#include <memory>

// Metoda wytwórcza do tworzenia obiektów
class Factory
{
public:
    virtual std::unique_ptr<Figure> create() = 0;
    virtual ~Factory() = default;
};

class SquareFactory : public Factory
{
public:
    std::unique_ptr<Figure> create() override {
    return std::make_unique<Square>();
    }
};

class RectangleFactory : public Factory
{
public:
   std::unique_ptr<Figure> create() override {
   return std::make_unique<Rectangle>();
   }
};

class TriangleFactory : public Factory
{
public:
    std::unique_ptr<Figure> create() override {
    return std::make_unique<Triangle>();
    }
};

class CircleFactory : public Factory
{
public:
    std::unique_ptr<Figure> create() override {
    return std::make_unique<Circle>();
    }
};

class RhombFactory : public Factory
{
public:
    std::unique_ptr<Figure> create() override {
    return std::make_unique<Rhomb>();
    }
};

class TrapezoidFactory : public Factory
{
public:
    std::unique_ptr<Figure> create() override {
    return std::make_unique<Trapezoid>();
    }
};

#endif // FACTORY_H
