#include "trapezoid.h"
#include <iostream>
using namespace std;

double Trapezoid::calculate()
{
    total = ((a + b) * h) / 2;
    return total;
}

void Trapezoid::get()
{
    cout << "Podstawa A trapezu: ";
    while (!(cin >> a)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
    cout << "Podstawa B trapezu: ";
    while (!(cin >> b)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
    cout << "Wysokość trapezu: ";
    while (!(cin >> h)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartoś! Podaj liczbę: ";
    }
}

string Trapezoid::fig_name()
{
    name = "trapez";
    return name;
}
