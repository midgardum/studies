#include "triangle.h"
#include <iostream>
using namespace std;

double Triangle::calculate()
{
    total = (a * h) / 2;
    return total;
}

void Triangle::get()
{
    cout << "Podstawa trójkąta: ";
    while (!(cin >> a)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
    cout << "Wysokość trójkąta: ";
    while (!(cin >> h)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
}

string Triangle::fig_name()
{
    name = "trójkąt";
    return name;
}
