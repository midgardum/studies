#include "rhomb.h"
#include <iostream>
using namespace std;

double Rhomb::calculate()
{
    total = (d1 * d2) / 2;
    return total;
}

void Rhomb::get()
{
    cout << "Przekątna D1 rombu: ";
    while (!(cin >> d1)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
    cout << "Przekątna D2 rombu: ";
    while (!(cin >> d2)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
}

string Rhomb::fig_name()
{
    name = "romb";
    return name;
}
