#include <iostream>
#include <vector>
#include <memory>
#include <fstream>
#include <cstdlib>
#include "square.h"
#include "rectangle.h"
#include "triangle.h"
#include "circle.h"
#include "rhomb.h"
#include "trapezoid.h"
#include "figure.h"
#include "factory.h"
using namespace std;

// Deklaracje funkcji
void menu();
void save(string, double);
void fig(Factory &obj);

int main()
{
    unique_ptr<Factory> obj;

    while (true) {
        menu();
        cout << "\nWybierz działanie, które chcesz wykonać: ";
        int option {};
        cin >> option;

        switch (option) {
        case 1:
            obj = make_unique<SquareFactory>();
            fig(*obj.get());
            break;
        case 2:
            obj = make_unique<RectangleFactory>();
            fig(*obj.get());
            break;
        case 3:
            obj = make_unique<TriangleFactory>();
            fig(*obj.get());
            break;
        case 4:
            obj = make_unique<CircleFactory>();
            fig(*obj.get());
            break;
        case 5:
            obj = make_unique<RhombFactory>();
            fig(*obj.get());
            break;
        case 6:
            obj = make_unique<TrapezoidFactory>();
            fig(*obj.get());
            break;
        case 7:
            exit(0);
            break;
        default:
            cout << "Nie ma takiej opcji!" << endl;
            break;
        }

        getchar();
        getchar();
        system("clear");
    }

    return 0;
}

// Definicje funkcji
void menu() {
    vector<string> menu = { "Kwadratu", "Prostokąta", "Trójkąta", "Koła", "Rombu", "Trapezu", "Koniec programu" };
    cout << "\nOBLICZ POLE: " << endl;
         for (unsigned int i = 0; i < menu.size(); i++) {
            cout << i + 1 << ". " << menu[i] << endl;
         }
}

void fig(Factory &obj) {
    auto shape = obj.create();
    shape -> get();
    double total = shape -> calculate();
    string name = shape -> fig_name();
    cout << "Pole figury " <<  name << " wynosi: " << total << endl;
    save(name, total);
}

void save(string name, double total) {

    fstream file;
    int option {};

    cout << "\nCzy chcesz zapisać wynik do pliku? \n1. Tak \n2. Nie\n";
    cin >> option;

    if (option == 1) {
        file.open("wyniki.txt", ios::in | ios::app);
        if (file.good() == false) {
            cerr << "Plik nie istnieje!";
            exit(0);
        } else {
            file << "Pole figury " << name << ": " << total << endl;
            cout << "\nGratulacje! Wynik został zapisany!" << endl;
        }
        file.close();
    }
    else {
        exit(0);
    }

    cout << "Czy chcesz wyświetlić wyniki? \n1. Tak \n2. Nie\n";
    cin >> option;
    cout << endl;

    if (option == 1) {
        file.open("wyniki.txt", ios::in | ios::app);
        string line {""};
        string display{""};

        while (getline(file, line)) {
            display = line;
            cout << display << endl;
        }
        file.close();
    }
}
