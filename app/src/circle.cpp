#include "circle.h"
#include <iostream>
using namespace std;

double Circle::calculate()
{
    total = 3.14 * r * r;
    return total;
}

void Circle::get()
{
    cout << "Promień koła: ";
    while (!(cin >> r)) {
        cin.clear();
        while (cin.get() != '\n')
            continue;
        cout << "Niepoprawna wartość! Podaj liczbę: ";
    }
}

string Circle::fig_name()
{
    name = "koło";
    return name;
}
