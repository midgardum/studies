#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

void square() {
    double a {};
    cout << "Podstawa kwadratu: "; cin >> a;
    cout << "Pole kwadratu wynosi: " << a * a << endl;
}

void rectangle() {
    double a {}, b {};
    cout << "Bok A prostokąta: "; cin >> a;
    cout << "Bok B prostokąta: "; cin >> b;
    cout << "Pole prostokąta wynosi: " << a * b << endl;
}

void triangle() {
    double a {}, h {};
    cout << "Podstawa trójkąta: "; cin >> a;
    cout << "Wysokość trójkąta: "; cin >> h;
    cout << "Pole trójkąta wynosi: " << (a * h) / 2 << endl;
}

void circle() {
    double r {};
    cout << "Promień koła: "; cin >> r;
    cout << "Pole koła wynosi: " << 3.14 * r * r << endl;
}

void rhomb() {
    double d1 {}, d2 {};
    cout << "Przekątna D1 rombu: "; cin >> d1;
    cout << "Przekątna D2 rombu: "; cin >> d2;
    cout << "Pole rombu wynosi: " << (d1 * d2) / 2 << endl;
}

void trapezoid() {
    double a {}, b {}, h {};
    cout << "Podstawa A trapezu: "; cin >> a;
    cout << "Podstawa B trapezu: "; cin >> b;
    cout << "Wysokość trapezu: "; cin >> h;
    cout << "Pole trapezu wynosi: " << ((a + b) * h) / 2 << endl;
}

void menu() {
    vector<string> menu = { "Kwadratu", "Prostokąta", "Trójkąta", "Koła", "Rombu", "Trapezu", "Koniec programu" };
    cout << "OBLICZ POLE: " << endl;
         for (unsigned int i = 0; i < menu.size(); i++) {
            cout << i + 1 << ". " << menu[i] << endl;
         }
}

int main()
{
    menu();

    int option;
    cout << "\nWybierz działanie, które chcesz wykonać: ";
    cin >> option;

    switch (option) {
        case 1: square(); break;
        case 2: rectangle(); break;
        case 3: triangle(); break;
        case 4: circle(); break;
        case 5: rhomb(); break;
        case 6: trapezoid(); break;
        case 7: exit(0);
        default: cout << "Nie ma takiej opcji!\n";
    }

    return 0;
}
