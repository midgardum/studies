wiadomosc = raw_input("Wpisz wiadomosc do zaszyfrowania: ")
przesuniecie = int(raw_input("Wpisz ile nalezy przesunac: "))

def szyfr(wiadomosc, przesuniecie):
    alfabet = list('abcdefghijklmnopqrstuvwxyz')
    szyfr = ''
    try:
        for i in wiadomosc:
            szyfr += alfabet[(alfabet.index(i) + przesuniecie) % (len(alfabet))]
    except:
        print("Blad programu. Mozesz wpisac tylko male litery lacinskiego alfabetu")
    return szyfr
print(szyfr(wiadomosc, przesuniecie))