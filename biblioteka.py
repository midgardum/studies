# coding: utf-8

class Biblioteka():
    # lista ksiazek
    def __init__ (self, lista):
        self.dostepne = lista

    # funkcje
    def wyswietl(self): # __wyswietl - skladnik prywatny 
        print("Dostpne książki:")
        for ksiazka in self.dostepne:
            print(ksiazka)
    def wypozycz(self, ksiazka):
        if ksiazka in self.dostepne:
            print("Pożyczyłeś książkę: {}".format(ksiazka)) # łączenie zamiast plusa
            self.dostepne.remove(ksiazka)
        else:
            print("Przykro mi, książka: {} jest niedostępna".format(ksiazka))
    def dodaj(self, zwracana):
        self.dostepne.append(zwracana)
        print("Zwróciłeś ksiązkę: {}".format(ksiazka))

class Klient():
    def wypozycz(self):
        print("Podaj tytuł książki:")
        self.ksiazka = raw_input()
        return self.ksiazka

    def oddaj(self):
        print("Podaj tytuł zwracanej książki:")
        self.ksiazka = raw_input()
        return self.ksiazka

# stworzenie biblioteki i listy książek
biblioteka = Biblioteka(["Janko Muzykant", "Kamizelka", "Potop"])

# stworzenie użytkownika biblioteka
klient_1 = Klient()

while True:
    # menu tekstowe dla użytkownika
    print(" ")
    print("Biblioteka:")
    print("1. Wyświetl dostępne książki")
    print("2. Wypożycz książkę")
    print("3. Zwróć książkę")
    print("4. Wyjdź z programu")

    # użytkownik dokonuje wyboru menu poprzez wpisanie liczby
    wybor = int(input())   
    if wybor is 1:
        biblioteka.wyswietl()
    if wybor is 2:
        ksiazka = klient_1.wypozycz()
        biblioteka.wypozycz(ksiazka)
    if wybor is 3:
        ksiazka = klient_1.oddaj()
        biblioteka.dodaj(ksiazka)
    if wybor is 4:
        quit()
