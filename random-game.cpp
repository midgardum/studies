#include <iostream>
#include <cstdlib>
#include <time.h>
using namespace std;

int main()
{
/*
W komputerach czas jest przechowywany jako liczba sekund od 1970 roku (czasu UTC)
Nasz program pobierze tę liczbę sekund na liczbę z przedziału 1 - 100
Ppotrzebujemy wzór na resztę z dzielenia, np.: 1229 % 100 = reszta 29
wyznaczona liczba losowa = liczba sekund od 1970 + liczbę stałą % zakres losowanych liczb (podanie największej liczby z zakresu + 1)
wzór: x1 = (s + b) % n
na jej podstawie druga liczba: x2 = (s * x1 + b) % n
na jej podstawie trzecia liczba: x3 = (s * x2 + b) % n
Liczby generowane w ten sposób nie są w pełni losowe, ponieważ wykazują cykliczność, dlatego nazywamy je liczbami psuedolosowymi
komputer wylosuje z zakresu - 1 (kompter liczy od 0), dlatego trzeba dodać +1
można też dzielić, aby otrzymać liczbę niecałkowitą
*/

    int user_number {0}, computer_number {0}, chances {0};
    srand(time(NULL));

    cout << "Witaj, pomyślałem sobie liczbę z zakresu 1 - 100. Zgadnij jaka to liczba." << endl;
    computer_number = rand() % 100 + 1;
    cout << "Wylosowałem: " << computer_number << endl;

    do {
        chances++;
        cout << "Twoja liczba to (" << chances << " próba): ";
        cin >> user_number;
        if (user_number > computer_number) {
            cout << "Za dużo!" << endl;
        } else if (user_number < computer_number) {
            cout << "Za mało!" << endl;
        }
    } while (user_number != computer_number);
    cout << "Gratulacje! Wylosowana liczba do while to " << computer_number
         << "! Wygrywasz w " << chances << " próbie. \n" << endl;

    return 0;
}
