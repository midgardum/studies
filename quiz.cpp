#include <iostream>
#include <fstream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <cctype>
using namespace std;

/* quiz:
zada użytkownikowi 5 pytań zczytanych z pliku a) b) c) d)
sprawdzi poprawność udzielonych odpowiedzi i wyświetli końcowy wynik punktowy
*/

int main()
{
    string topic {""}, nick {""};
    string question[5], option_a[5], option_b[5], option_c[5], option_d[5], answer {""}, correct[5];
    int score {0};

    string line {""}; // zmienna do przechowywania linii
    int line_number {1}; // licznik linii
    int question_number {0};

    fstream quiz;
    quiz.open("quiz.txt", ios:: in);

    if (quiz.good() == false) {
        cout << "Plik nie istnieje!";
        exit(0);
    }

    while (getline(quiz, line)) {
        switch (line_number) {
            case 1: topic = line; break;
            case 2: nick = line; break;
            case 3: question[question_number] = line; break;
            case 4: option_a[question_number] = line; break;
            case 5: option_b[question_number] = line; break;
            case 6: option_c[question_number] = line; break;
            case 7: option_d[question_number] = line; break;
            case 8: correct[question_number] = line;  break;
        }

        if (line_number == 8) {
            line_number = 2;
            question_number++;
        }
        line_number++;
    }

    quiz.close();

    cout << topic << endl << endl;

    for (int i = 0; i <= 4; i++) {
        cout << question[i] << endl;
        cout << "A. " << option_a[i] << endl;
        cout << "B. " << option_b[i] << endl;
        cout << "C. " << option_c[i] << endl;
        cout << "D. " << option_d[i] << endl;
        cout << "Twoja odpowiedź: ";
        cin >> answer;

        // przekonwertowanie na małe litery
        transform(answer.begin(), answer.end(), answer.begin(), ::tolower);

        if (answer == correct[i]) {
            cout << "Dobrze! Zdobywasz punkt!" << endl << endl;
            score++;
        } else {
            cout << "Źle! Brak punktu! Poprawna odpowiedź: " << correct[i] << endl << endl;
        }
    }

    cout << "Koniec quizu! Zdobyte punkty: " << score << endl;

    if (score <= 2) {
        cout << "Weź się do nauki!" << endl;
    } else if (score >= 3 && score <= 4) {
        cout << "Nie jest źle, choć mogło być lepiej" << endl;
    } else {
        cout << "Grarulacje! Świetnie ci poszło!" << endl;
    }

    return 0;
}
