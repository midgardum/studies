#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

using namespace std;

int main()
{
    // Stwórz gniazdo
    int listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
        cerr << "Nie można stworzyć gniazda! Wyjście" << endl;
        return -1;
    }

    // Połącz adres ip i port z gniazdem
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(54000);
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr);

    bind(listening, (sockaddr*)&hint, sizeof(hint));

    // Powiedz, że gniazdo nasłuchuje
    listen(listening, SOMAXCONN);

    // Poczekaj na połączenie
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);

    int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
    char host[NI_MAXHOST];      // Nazwa serwera
    char service[NI_MAXSERV];   // Port na któ©ym klient jest połączony

    memset(host, 0, NI_MAXHOST);
    memset(service, 0, NI_MAXSERV);

    if (getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, service, NI_MAXSERV, 0) == 0) {
        cout << host << " połączony na porcie " << service << endl;
    }
    else {
        inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
        cout << host << " połączony na porcie " << ntohs(client.sin_port) << endl;
    }

    // Zamknięcie gniazda
    close(listening);

    // Wyświetl wiadomość
    char buf[4096];
    while (true) {
        memset(buf, 0, 4096);

        // Poczekaj na wysłanie danych
        int bytesReceived = recv(clientSocket, buf, 4096, 0);
        if (bytesReceived == -1) {
            cerr << "Problem z połączeniem" << endl;
            break;
        }
        if (bytesReceived == 0) {
            cout << "Przerwane połączenie" << endl;
            break;
        }
        cout << string(buf, 0, bytesReceived) << endl;

        // Prześlij ponownie wiadomość
        send(clientSocket, buf, bytesReceived + 1, 0);
    }

    // Zamknij gniazdo
    close(clientSocket);

    return 0;
}
