#include <iostream>
using namespace std;

// stworzenie klasy meble
class furniture {
    // modyfikator dostępu public
    public:
        // publiczne funkcje składowe (metody)
        void print_info();
        void discount(int year);
        void delivery(int price);
        // publiczne zmienne składowe (pola)
        string model;
        int year;
        int price;
        int height;
        int lenght;
        int width;
        // deklaracja konstruktora
        furniture();
    // modyfikator dostępu private
    private:
        // prywatne zmienne składowe
        bool gift = true;
        int treshold = 0;
        // prywatna funkcja składowa
        void free(int treshold);
};

// definicja konstruktora
furniture::furniture() {
    string model {""};
    int year {0};
    int price {0};
    int height {0};
    int lenght {0};
    int width {0};
}

// implemenaja funkcji wyświetlającej informacje o produkcie
void furniture::print_info() {
    cout << "\n---------------- \n"
         << "Informacje o produkcie:" << endl
         << "Nazwa modelu: " << model << endl
         << "Rok produkcji: " << year << endl
         << "Cena: " << price << " zł" << endl
         << "Wysokość: " << height << " cm" << endl
         << "Długość: " << lenght << " cm" << endl
         << "Szerokość: " << width << " cm" << endl;
}
// implementacja funkcji przydzielającej zniżkę dla produktu
void furniture::discount(int year) {
    if (year >= 2018) {
        cout << "Brak zniżki";
    } else if ((year <= 2017) && (year >= 2015)) {
        cout << "Zniżka: 20%";
    } else if ((year <= 2014) && (year >= 2000)) {
        cout << "Zniżka: 40%";
    } else cout << "Zniżka: 60%";
}

// implementacja funkcji obliczającej próg dla produktu
void furniture::delivery(int price) {
    treshold += price;
    free(treshold);
}

// implementacja funkcji decydującej o przyznaniu darmowej dostawy i upominka
void furniture::free(int treshold) {
    if (treshold >= 1500) {
        cout << "\nDarmowa dostawa" << endl
             << gift << " upominek";
    } else cout << "\nPłatna dostawa";
}

int main() {

    // stworzenie obiektów
    furniture wardrobe;
    furniture bed;
    furniture cabinet;

    // dostęp do zmiennych klasy meble
    // przypisanie wartości dla zmiennych poszczególnych obiektów
    wardrobe.model = "Szafa Classic";
    wardrobe.year = 2018;
    wardrobe.price = 1500;
    wardrobe.height = 230;
    wardrobe.lenght = 280;
    wardrobe.width = 80;

    bed.model = "Łóżko Classic";
    bed.year = 2016;
    bed.price = 2300;
    bed.height = 50;
    bed.lenght = 200;
    bed.width = 100;

    cabinet.model = "Szafka Classic";
    cabinet.year = 2011;
    cabinet.price = 500;
    cabinet.height = 50;
    cabinet.lenght = 60;
    cabinet.width = 40;

    // wywołanie funkcji
    wardrobe.print_info();
    wardrobe.furniture::discount(wardrobe.year);
    wardrobe.furniture::delivery(wardrobe.price);

    bed.print_info();
    bed.furniture::discount(bed.year);
    bed.furniture::delivery(bed.price);

    cabinet.print_info();
    cabinet.furniture::discount(cabinet.year);
    cabinet.furniture::delivery(cabinet.price);

    return 0;
}
