#include <iostream>
#include <unistd.h> // do sleep
#include <time.h> // do random
using namespace std;

int main() {

    // symulacja dużego lotka
    int number {0};

    cout << "Witaj w losowaniu! Za 3 sekundy nastąpi zwolnienie blokady. \n" << endl;
    sleep(3);
    srand(time(NULL));

    for (int i = 1; i <= 6; i++) {
        number = rand() % 49 + 1;
        sleep(1);
        cout << number << "\a" << endl;
    }

    return 0;
}
