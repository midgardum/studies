#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <string.h>
#include <string>

using namespace std;

int main()
{
    // Stwórz gniazdo
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        return 1;
    }

    int port = 54000;
    string ip = "127.0.0.1";
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(port);
    inet_pton(AF_INET, ip.c_str(), &hint.sin_addr);

    // Połącz z serwerem
    int connection = connect(sock, (sockaddr*)&hint, sizeof(hint));
    if (connection == -1) {
        return 1;
    }

    char buf[4096];
    string userInput;

    do {
        cout << "> ";
        getline(cin, userInput);

        // Wyśli na serwer
        int sending = send(sock, userInput.c_str(), userInput.size() + 1, 0);
        if (sending == -1) {
            cout << "Nie można wysłać na serwer!";
            continue;
        }

        // Czekaj na odpowiedź
        memset(buf, 0, 4096);
        int bytesReceived = recv(sock, buf, 4096, 0);
        if (bytesReceived == -1) {
            cout << "Błąd połączenia";
        } else {
            // Wyświetlenie odpowiedzi
            cout << "Serwer >>" << string(buf, bytesReceived) << "\r\n";
        }
     } while(true);

    close(sock);
    return 0;
}
